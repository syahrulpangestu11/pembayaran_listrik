<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tampilan_admin extends CI_Controller {

	public function index()
	{
		$data['konten']="v_admin";
		$this->load->model('admin_model', 'admin');
		$this->load->model('level_model','level');
		$data['data_admin']=$this->admin->get_admin();
		$data['data_level']=$this->level->get_level();
		$data['judul']="Admin";
		$this->load->view('template_admin', $data);
	}

	public function simpan_admin()
	{
		$this->form_validation->set_rules('nama_admin', 'Nama admin', 'trim|required',
		array('required' => 'nama admin harus diisi'));
		$this->form_validation->set_rules('username', 'Username', 'trim|required',
		array('required' => 'Username harus diisi'));
		$this->form_validation->set_rules('password', 'Password', 'trim|required',
		array('required' => 'Password harus diisi'));
		$this->form_validation->set_rules('id_level', 'Nama Level', 'trim|required',
		array('required' => 'Nama level harus diisi'));

		if ($this->form_validation->run() == TRUE) {
			$this->load->model('admin_model','admin');
			$masuk=$this->admin->masuk_db();
			if($masuk==true){
				$this->session->set_flashdata('pesan', 'sukses masuk');
			} else {
				//$this->session->set_flashdata('pesan', 'gagal masuk');
			}
			redirect(base_url('index.php/tampilan_admin'),'refresh');
		} else {
			$this->session->set_flashdata('pesan', validation_errors());
			redirect(base_url('index.php/tampilan_admin'),'refresh');

		}
	}

	public function get_detail_admin($id_admin='')
	{
		$this->load->model('admin_model');
		$data_detail=$this->admin_model->detail_admin($id_admin);
		echo json_encode($data_detail);
	}

	public function update_admin()
	{
		$this->form_validation->set_rules('nama_admin', 'nama admin', 'trim|required',array("required"=>"Nama lvel harus diisi"));
		$this->form_validation->set_rules('username', 'username', 'trim|required',array("required"=>"Nama lvel harus diisi"));
		$this->form_validation->set_rules('password', 'password', 'trim|required',array("required"=>"Nama lvel harus diisi"));
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', validation_errors());
			redirect(base_url('index.php/tampilan_admin'),'refresh');
		} else {
			$this->load->model('admin_model');
			$proses_update=$this->admin_model->update_admin();
			if($proses_update == TRUE){
				$this->session->set_flashdata('pesan', 'sukses update');
			} else {
				$this->session->set_flashdata('pesan', 'gagal update');
			}
			redirect(base_url('index.php/tampilan_admin'),'refresh');
		}
	}

	public function hapus_admin($id_admin='')
	{
		$this->load->model('admin_model','admin');
		$hapus=$this->admin->hapus_admin($id_admin);
		if($hapus){
			$this->session->set_flashdata('pesan', 'sukses hapus data');
			} else {
				$this->session->set_flashdata('pesan', 'gagal hapus data');
			}
			redirect(base_url('index.php/tampilan_admin'),'refresh');
	}

}

/* End of file Tampilan_admin.php */
/* Location: ./application/controllers/Tampilan_admin.php */