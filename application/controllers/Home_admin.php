<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_admin extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if($this->session->userdata('login_admin')!=TRUE){
				redirect('admin','refresh');
			}
	}

	public function index()
	{
		$data['konten']="welcome_message";
		$data['judul']="Selamat Datang";
		$this->load->view('template_admin',$data);
	}

}

/* End of file Home_admin.php */
/* Location: ./application/controllers/Home_admin.php */