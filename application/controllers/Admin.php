<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index()
	{
		$this->load->view('v_login_admin');
	}#ini adalah funtion untuk menampilkan viewnya atau default viewnya

	public function cek_login_admin()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required',array('required'=>'Username harus diisi'));
		$this->form_validation->set_rules('password', 'Password', 'trim|required',array('required'=>'Password harus diisi'));
		if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect(base_url('index.php/admin'));
			} else {
				$this->load->model('login_model');
				$cek_login=$this->login_model->get_login();
				if($cek_login->num_rows()>0){
					$data_login=$cek_login->row();
					$array = array(
						'id_admin' => $data_login->id_admin,
						'username'=>$data_login->username,
						'password'=>$data_login->password,
						'nama'=>$data_login->nama_admin,
						'login_admin'=>true,
						'id_level'=>$data_login->id_level
					);
					
					$this->session->set_userdata( $array );
					redirect(base_url('index.php/home_admin'));
				} else {
					$this->session->set_flashdata('pesan', 'username dan password tidak cocok');
					redirect(base_url('index.php/admin'));
				}
			}	
	}

	public function hapus_admin($id_admin='')
	{
		$this->load->model('admin_model','admin');
		$hapus=$this->admin->hapus_admin($id_admin);
		if($hapus){
			$this->session->set_flashdata('pesan', 'sukses hapus data');
			} else {
				
				$this->session->set_flashdata('pesan', 'gagal hapus data');
			}
			redirect(base_url('index.php/tampilan_admin'),'refresh');
	}
#ini proses untuk login ke halaman admin/ bagian manager,teller,dan lapangan itu
	#ini adalah login untuk sisi admin
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('admin','refresh');
	}
#ingat ini untuk sisi admin yaa, ada perbedaan untuk sisi admin dan pelanggan biasa, karena di sisi admin masih ada pengelompokan 3 level itu okeee
}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */