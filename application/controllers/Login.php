<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('tarif_model','tarif');
	}

	public function index()
	{
		if ($this->session->userdata('login')==TRUE) {
			redirect('user','refresh');
		}
		else{
			$data['tarif']=$this->tarif->get_tarif();
			$this->load->view('login',$data);# buatlah view seperti <- login itu tuuuu
		}
	}

	#pertama kalian membuat controller untuk login seperti function yang diatas yaitu function index itu, setelah itu

	public function proses_login()
	{
		if ($this->input->post('login')) {
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			if ($this->form_validation->run() == TRUE ) {
				$this->load->model('m_user');
				if ($this->m_user->get_login()->num_rows()>0) {
					$data=$this->m_user->get_login()->row();
					$array = array(
						'login' => TRUE,
						'nama_pelanggan'=>$data->nama_pelanggan,
						'username'=>$data->username,
						'password'=>$data->password,
						'id_pelanggan'=>$data->id_pelanggan
						 );
					$this->session->set_userdata($array);
					redirect('user','refresh');
				}
				else {
				$this->session->set_flashdata('pesan', 'salah username atau password');
				redirect('login','refresh');
				}
			}
		}else{
				$this->session->set_flashdata('pesan', validation_errors());
			redirect('login','refresh');
		}
	}

	public function simpan()
	{
		$this->load->model('m_user','m');
		$cek_data=$this->m->tambah_user();
		if($cek_data){
			$this->session->set_flashdata('pesan', '<div class="alert alert-success">Pendaftaran anda sukses</div>');
			redirect(base_url('index.php/login'),'refresh');
		} else {
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Pendaftaran anda gagal</div>');
			redirect(base_url('index.php/login'),'refresh');
		}
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */